#-------------------------------------------------
#
# Project created by QtCreator 2017-06-08T10:35:11
#
#-------------------------------------------------

QT       -= gui
TARGET = BatClassifyPluginRun
TEMPLATE = app

SOURCES += batclassifyplugin.cpp \
    speciesautoid.cpp \
    main.cpp

HEADERS += batclassifyplugin.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
