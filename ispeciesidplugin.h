#ifndef ISPECIESIDPLUGIN_H
#define ISPECIESIDPLUGIN_H

#include <QMap>
#include <QString>

class ISpeciesIdPlugin
{
public:
    virtual ~ISpeciesIdPlugin() {}

    virtual bool getSpecies(const QString &filename,
                            QMap<QString, double> &results) = 0;
    virtual QString errorMessage() const = 0;
};

#endif // ISPECIESIDPLUGIN_H
