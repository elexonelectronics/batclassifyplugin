#include "speciesautoid.h"
#include <QProcess>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>
#include <QDir>

bool SpeciesAutoId::getSpecies(const QString &filename, QMap<QString, double> &results)
{
    QDir dir("plugins/BatClassify/");

    if (!dir.exists()) {
        err_msg = "plugin not installed";
        return false;
    }

    QProcess batClassify;
    batClassify.setWorkingDirectory(dir.path());
    batClassify.start(dir.filePath("BatClassify.exe"), QStringList() << filename);

    if(!batClassify.waitForFinished()) {
        err_msg = "Timeout";
        return false;
    }

    QByteArray data = batClassify.readAll();
    QJsonDocument itemDoc = QJsonDocument::fromJson(data);

    if(itemDoc.isNull()) {
        err_msg = "BatClassify.exe results in bad format";
        return false;
    }

    QJsonObject jsonObject = itemDoc.object();
     for (QJsonObject::iterator it = jsonObject.begin(); it != jsonObject.end(); it++) {
         qDebug() << it.key() << it.value().toDouble();
         results.insert(it.key(), it.value().toDouble());
     }

    err_msg = QString();
    return true;
}

QString SpeciesAutoId::errorMessage() const
{
    return err_msg;
}
