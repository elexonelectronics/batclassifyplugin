#ifndef BATCLASSIFYPLUGIN_H
#define BATCLASSIFYPLUGIN_H

#include <QObject>
#include "ianabatinsightplugin.h"
#include "speciesautoid.h"

class BatClassifyPlugin : public QObject, public IAnabatInsightPluginV1
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID ANABAT_INSIGHT_PLUGIN_V1_IID)
    Q_INTERFACES(IAnabatInsightPluginV1)
public:
    QString name();
    QString version();
    QString author();
    QString description();
    QString website();
    QStringList speciesIdServices();
    ISpeciesIdPlugin *getSpeciesIdInstance(const QString &name);
};

#endif // BATCLASSIFYPLUGIN_H
