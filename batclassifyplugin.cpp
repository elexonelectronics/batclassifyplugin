#include "batclassifyplugin.h"

QString BatClassifyPlugin::name()
{
    return "BatClassify";
}
QString BatClassifyPlugin::version()
{
    return "1.0";
}
QString BatClassifyPlugin::author()
{
    return "Titley Scientific";
}
QString BatClassifyPlugin::description()
{
    return "BatClassify is an open-source auto id software covering a subset of UK bat species.";
}

QString BatClassifyPlugin::website()
{
    return "https://bitbucket.org/elexonelectronics/";
}

QStringList BatClassifyPlugin::speciesIdServices()
{
    return QStringList() << "BatClassify";
}

ISpeciesIdPlugin *BatClassifyPlugin::getSpeciesIdInstance(const QString &name)
{
    if(name == QString("BatClassify"))
        return new SpeciesAutoId();

    return NULL;
}
