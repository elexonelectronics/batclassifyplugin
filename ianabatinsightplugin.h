#ifndef IANABATINSIGHTPLUGIN_H
#define IANABATINSIGHTPLUGIN_H

#include <QString>
#include "ispeciesidplugin.h"

class IAnabatInsightPluginV1
{
public:
    virtual ~IAnabatInsightPluginV1() {}
    virtual QString name() = 0;
    virtual QString version() = 0;
    virtual QString author() = 0;
    virtual QString description() = 0;
    virtual QString website() = 0;

    virtual QStringList speciesIdServices() = 0;
    virtual ISpeciesIdPlugin *getSpeciesIdInstance(const QString &name) = 0;
};

QT_BEGIN_NAMESPACE
#define ANABAT_INSIGHT_PLUGIN_V1_IID "com.titley.insight.plugin.v1"
Q_DECLARE_INTERFACE(IAnabatInsightPluginV1, ANABAT_INSIGHT_PLUGIN_V1_IID)
QT_END_NAMESPACE

#endif // IANABATINSIGHTPLUGIN_H
