#ifndef SPECIESAUTOID_H
#define SPECIESAUTOID_H

#include "ispeciesidplugin.h"

class SpeciesAutoId : public ISpeciesIdPlugin
{
public:
    bool getSpecies(const QString &filename, QMap<QString, double> &results);
    QString errorMessage() const;

private:
    QString err_msg;
};

#endif // SPECIESAUTOID_H
