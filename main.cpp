#include <QCoreApplication>
#include <QDebug>

#include "speciesautoid.h"

SpeciesAutoId autoId;

// Main is just for debugging purposes. Will be a library later.
int main(int argc, char *argv[])
{
    Q_UNUSED(argc);
    Q_UNUSED(argv);

    QString filename = "bat.wav";
    QMap<QString, double> results;
    if (autoId.getSpecies(filename, results))
        qDebug() << results;
    else
        qDebug() << "error" << autoId.errorMessage();

    return 0;
}

